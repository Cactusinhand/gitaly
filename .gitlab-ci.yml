stages:
  - build
  - test

default:
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-2.7-golang-1.15-git-2.31
  tags:
    - test

variables:
  DOCKER_DRIVER: overlay2
  GIT_VERSION: "v2.33.0"
  GO_VERSION: "1.16"
  RUBY_VERSION: "2.7"
  POSTGRES_VERSION: "12.6-alpine"

include:
  - template: Workflows/MergeRequest-Pipelines.gitlab-ci.yml
  - template: Security/Dependency-Scanning.gitlab-ci.yml


.cache: &cache_definition
  cache:
    key:
      files:
        - Makefile
        - ruby/Gemfile.lock
      prefix: git-${GIT_VERSION}-ruby-${RUBY_VERSION}
    paths:
      - _build/deps
      - _build/Makefile.sha256
      - ruby/vendor/bundle

.test_template: &test_definition
  <<: *cache_definition
  stage: test
  # Override the cache definition for pull
  cache:
    key:
      files:
        - Makefile
        - ruby/Gemfile.lock
      prefix: git-${GIT_VERSION}-ruby-${RUBY_VERSION}
    paths:
      - _build/deps
      - _build/Makefile.sha256
      - ruby/vendor/bundle
    policy: pull
  artifacts:
    paths:
    - ruby/tmp/gitaly-rspec-test.log
    when: on_failure
    expire_in: 1 week

.postgres_template: &postgres_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.31
  services:
    - postgres:${POSTGRES_VERSION}
  variables: &postgres_variables
    PGHOST: postgres
    PGPORT: "5432"
    PGUSER: postgres
    POSTGRES_HOST_AUTH_METHOD: trust
  before_script:
    - go version
    - git version
    - while ! psql -h $PGHOST -U $PGUSER -c 'SELECT 1' > /dev/null; do echo "awaiting Postgres service to be ready..." && sleep 1 ; done && echo "Postgres service is ready!"


verify:
  <<: *cache_definition
  stage: test
  script:
    - make verify

proto:
  <<: *cache_definition
  stage: test
  script:
    - make proto no-changes
  artifacts:
    paths:
    - _build/proto.diff
    - ruby/proto/gitaly/*
    - proto/go/gitalypb/*
    when: on_failure

build:
  <<: *cache_definition
  stage: build
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.31
  script:
    - go version
    - make all git
    - _support/test-boot .
  parallel:
    matrix:
      - GO_VERSION: ["1.16" ]
        GIT_VERSION: ["v2.29.0", "v2.30.0", "v2.31.0", "v2.32.0", "v2.33.0" ]
        RUBY_VERSION: [ "2.7" ]

binaries:
  <<: *cache_definition
  stage: build
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.31
  only:
    - tags
  script:
    # Just in case we start running CI builds on other architectures in future
    - go version
    - make build
    - cd _build && sha256sum bin/* | tee checksums.sha256.txt
  artifacts:
    paths:
    - _build/checksums.sha256.txt
    - _build/bin/
    name: "${CI_JOB_NAME}:go-${GO_VERSION}-git-${GIT_VERSION}"
    expire_in: 6 months
  parallel:
    matrix:
      - GO_VERSION: ["1.16" ]

test:
  <<: *test_definition
  <<: *postgres_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.31
  script:
    - _build/deps/git/install/bin/git version
    # This command will make all directories except of our build directory and Ruby code unwritable.
    # The purpose is to verify that there is no test which writes into those directories anymore, as
    # they should all instead use a temporary directory for runtime data.
    - find . -type d \( -path ./_build -o -path ./ruby \) -prune -o -type d -exec chmod a-w {} \;
    - make ${TARGET}
  artifacts:
    paths:
      - _build/reports/go-tests-report-go-${GO_VERSION}-git-${GIT_VERSION}.xml
    reports:
      junit: _build/reports/go-tests-report-go-${GO_VERSION}-git-${GIT_VERSION}.xml
  parallel:
    matrix:
      - GO_VERSION: ["1.16" ]
        GIT_VERSION: ["v2.29.0", "v2.30.0", "v2.31.0", "v2.32.0", "v2.33.0", "v2.33.0-rc0", "v2.33.0-rc1", "v2.33.0-rc2"]
        TARGET: test
      - GO_VERSION: "1.16"
        TARGET: [ test-with-proxies, test-with-praefect, race-go ]

nightly:git:
  <<: *test_definition
  <<: *postgres_definition
  image: registry.gitlab.com/gitlab-org/gitlab-build-images:ruby-${RUBY_VERSION}-golang-${GO_VERSION}-git-2.31
  script:
    - go version
    - make all ${TARGET} GIT_PATCHES=
  parallel:
    matrix:
      - GO_VERSION: "1.16"
        GIT_VERSION: ["master", "next"]
        TARGET: [ test, test-with-proxies, test-with-praefect ]
  rules:
    - if: '$CI_PIPELINE_SOURCE == "schedule"'


gemnasium-dependency_scanning:
  rules:
    - if: $DEPENDENCY_SCANNING_DISABLED
      when: never
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_MERGE_REQUEST_IID
    - if: $CI_COMMIT_TAG

praefect_sql_connect:
  <<: *test_definition
  services:
    - postgres:${POSTGRES_VERSION}
  variables:
    POSTGRES_DB: praefect_test
    POSTGRES_USER: praefect
    POSTGRES_PASSWORD: sql-password
  script:
    - make
    # Sanity check: direct ping with psql
    - PGPASSWORD=$POSTGRES_PASSWORD psql -h postgres -U $POSTGRES_USER -d $POSTGRES_DB -c 'select now()'
    - ruby -rerb -e 'ERB.new(ARGF.read).run' _support/config.praefect.toml.ci-sql-test.erb > config.praefect.toml
    - ./_build/bin/praefect -config config.praefect.toml sql-ping
    - ./_build/bin/praefect -config config.praefect.toml sql-migrate


backwards_compatibility_test:
  # The job verifies the old version of the service could run its database migration on the database where the
  # migration of the newest service version was already applied.
  <<: *test_definition
  <<: *postgres_definition
  rules:
    - changes:
      - "internal/praefect/datastore/migrations/*"
  script:
    - git fetch origin 'refs/tags/*:refs/tags/*'
    - git checkout $(_support/get-previous-minor-release)
    - git checkout --no-overlay $CI_COMMIT_SHA -- internal/praefect/datastore/migrations
    - make test-postgres
